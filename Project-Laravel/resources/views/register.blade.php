<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook | Daftar</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome"method="POST">
        @csrf
        <label for="Fname">First name:</label><br>
        <input type="text" name="Fname" id="Fname" required><br><br>
        <label for="Lname">Last name:</label><br>
        <input type="text" name="Lname" id="Lname" required><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" id="male" value="male" required>
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">Other</label><br><br>
        <label for="nasionality">Nasionality:</label><br><br>
        <Select name="nasionality" id="nasionality" >
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapura</option>
        </Select><br><br>
        <label for="lang-spoken">Language Spoken:</label><br><br>
        <input type="checkbox" name="lang-spoken" id="b-indo" required>
        <label for="b-indo">Bahasa Indonesia</label><br>
        <input type="checkbox" name="lang-spoken" id="b-ing">
        <label for="b-ing">English</label><br>
        <input type="checkbox" name="lang-spoken" id="b-othr">
        <label for="b-othr">Other</label><br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <button >Sign Up</button>
    </form>
</body>
</html>