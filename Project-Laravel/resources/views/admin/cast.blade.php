@extends('admin.master')

@section('li-cast')
    active
@endsection



@push('styles')
<!-- Data Tables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css"/>
@endpush

@push('scripts')
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>    
@endpush

@section('content')
<section class="content">

    <div class="card">
        <div class="card-header">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
          <h3 class="card-title">Cast Data Table</h3><br>
          <a href="/cast/create" class="btn btn-success add" ><i class="fas fa-plus"></i> Create Cast</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($casts as $key=>$cast)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $cast->nama }}</td>
                <td>{{ $cast->umur }}</td>
                <td>{{ $cast->bio }}</td>
                <td class="text-center form-inline">
                    <a href="/cast/detail/{{ $cast->id }}" class="btn btn-info btn-sm mr-2" ><i class="fas fa-eye"></i> View</a>
                    <a href="/cast/{{ $cast->id }}/edit" class="btn btn-primary btn-sm mr-2" ><i class="fas fa-edit"></i> Edit</a>
                    <form action="/cast/detail/{{$cast->id}}" method="POST" >
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Hapus" id=hapus class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
            @endforeach
            
            </tbody>
            <tfoot>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
</section>
@endsection