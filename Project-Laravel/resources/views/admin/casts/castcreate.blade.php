@extends('admin.master')

@section('li-cast')
    active
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Create Cast</h3>
                    </div>
                    
                    <form role="form" action="/cast" method="POST">
                        @csrf
                    <div class="card-body">
                        <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" name="nama"class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Enter Cast Name" value="{{ old('nama','') }}" required>
                        @error('nama')<p class="text-danger">Please Input Name</p> @enderror
                        </div>
                    <div class="form-group">
                        <label for="umur">Age</label>
                        <input type="text" name="umur"class="form-control col-3 @error('umur') is-invalid @enderror" id="umur" placeholder="Enter Cast Age" value="{{ old('umur','') }}" required>
                        @error('umur')<p class="text-danger">Please Input Age</p> @enderror
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea name="bio" id="bio" class="form-control @error('bio') is-invalid @enderror" rows="10" placeholder="Enter Cast Bio" value="{{ old('bio','') }}" required></textarea>
                        @error('bio')<p class="text-danger">Please Input Bio</p> @enderror
                    </div>
                    
                    <div class="card-footer ">
                    <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection