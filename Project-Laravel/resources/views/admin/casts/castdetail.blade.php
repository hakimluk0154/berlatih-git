@extends('admin.master')

@section('li-cast')
    active
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h3>Cast Detail</h3>
              </div>
            </div>
          </div>
        <div class="card card-widget widget-user-2">

            <div class="widget-user-header bg-primary">
                <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('adminlte/dist/img/avatar5.png') }}" alt="User Avatar">
                </div>
                
                <h3 class="widget-user-username">{{ $casts->nama }}</h3>
                <h5 class="widget-user-desc">{{ $casts->umur }} Tahun</h5>
            </div>
            <div class="card-body ">
                <h4>Biodata Singkat:</h4>
                <p>{{ $casts->bio }}</p>
                <a href="/cast" class="btn btn-primary" >Kembali</a>
            </div>
            </div>
    </div>
</section>
@endsection