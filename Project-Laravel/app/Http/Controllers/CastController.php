<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        // dd($casts);
        return view('admin.cast',compact('casts'));
    }
    public function create()
    {
        return view('admin.casts.castcreate');
    }
    public function store(Request $request)
        {
            $request->validate([
                'nama'=> 'required',
                'umur'=> 'required',
                'bio' =>'required',
            ]);

            $query = DB::table('casts')->insertGetId([

                'nama'=> $request["nama"],
                'umur'=> $request["umur"],
                'bio'=>$request["bio"]
            ]);

            return redirect('/buku')->with('success','Buku Berhasil Ditambahkan');
        }
    
    public function show($cast_id)
    {
        $casts=DB::table('casts')->where('id',$cast_id)->first();
        return view('admin.casts.castdetail', compact('casts'));
    }
    public function edit($cast_id)
    {
        $casts=DB::table('casts')->where('id',$cast_id)->first();
        return view('admin.casts.castedit', compact('casts'));
    }
    public function update($cast_id, Request $request)
    {
        $request->validate([
            'nama'=> 'required',
            'umur'=> 'required',
            'bio' =>'required',
        ]);
        $query=DB::table('casts')
        ->where('id',$cast_id)
        ->update([
            'nama'=> $request["nama"],
            'umur'=> $request["umur"],
            'bio'=>$request["bio"]
        ]);
        return redirect('/cast')->with('success','Data Berhasil Di Edit');
    }
    public function destroy($cast_id)
    {
        $query=DB::table('casts')
        ->where('id',$cast_id)
        ->delete();
        return redirect('/cast')->with('success','Data Berhasil Di Hapus');
    }
}
